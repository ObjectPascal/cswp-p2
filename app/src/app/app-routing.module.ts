import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { EditComponent } from './pages/edit/edit.component';
import { EditinsuranceComponent } from './pages/editinsurance/editinsurance.component';
import { EditinsurerComponent } from './pages/editinsurer/editinsurer.component';
import { EditPolicyHolderComponent } from './pages/editpolicyholder/editpolicyholder.component';
import { HomeComponent } from './pages/home/home.component';
import { InsuranceComponent } from './pages/insurance/insurance.component';
import { InsurerComponent } from './pages/insurer/insurer.component';
import { LoginComponent } from './pages/login/login.component';
import { PolicyholderComponent } from './pages/policyholder/policyholder.component';
import { UserComponent } from './pages/user/user.component';
import { AuthService } from './services/auth/auth.service';
import { CacheInterceptorService } from './services/http/cacheInterceptor.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', pathMatch: 'full', component: HomeComponent },
  {
    path: 'about',
    pathMatch: 'full',
    component: AboutComponent,
  },
  { path: 'login', pathMatch: 'full', component: LoginComponent },
  {
    path: 'users',
    pathMatch: 'full',
    component: UserComponent,
    canActivate: [AuthService],
  },
  {
    path: 'users/new',
    pathMatch: 'full',
    component: EditComponent,
    canActivate: [AuthService],
  },
  {
    path: 'users/edit',
    pathMatch: 'full',
    redirectTo: 'users/new',
  },
  {
    path: 'users/edit/:id',
    pathMatch: 'full',
    component: EditComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurers',
    pathMatch: 'full',
    component: InsurerComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurers/new',
    pathMatch: 'full',
    component: EditinsurerComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurers/edit',
    pathMatch: 'full',
    component: EditinsurerComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurers/edit/:id',
    pathMatch: 'full',
    component: EditinsurerComponent,
    canActivate: [AuthService],
  },
  {
    path: 'policyholders',
    pathMatch: 'full',
    component: PolicyholderComponent,
    canActivate: [AuthService],
  },
  {
    path: 'policyholders/new',
    pathMatch: 'full',
    component: EditPolicyHolderComponent,
    canActivate: [AuthService],
  },
  {
    path: 'policyholders/edit',
    pathMatch: 'full',
    component: EditPolicyHolderComponent,
    canActivate: [AuthService],
  },
  {
    path: 'policyholders/edit/:id',
    pathMatch: 'full',
    component: EditPolicyHolderComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurances',
    pathMatch: 'full',
    component: InsuranceComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurances/new',
    pathMatch: 'full',
    component: EditinsuranceComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurances/edit',
    pathMatch: 'full',
    component: EditinsuranceComponent,
    canActivate: [AuthService],
  },
  {
    path: 'insurances/edit/:id',
    pathMatch: 'full',
    component: EditinsuranceComponent,
    canActivate: [AuthService],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
