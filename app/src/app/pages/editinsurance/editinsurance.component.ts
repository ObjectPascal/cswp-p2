import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InsuranceService } from 'src/app/services/insurance/insurance.service';
import { InsurerService } from 'src/app/services/insurer/insurer.service';
import { InsuranceComponent } from '../insurance/insurance.component';
import { Insurance } from '../insurance/insurance.model';
import { Insurer } from '../insurer/insurer.model';

@Component({
  selector: 'app-editinsurance',
  templateUrl: './editinsurance.component.html',
  styleUrls: ['./editinsurance.component.css'],
})
export class EditinsuranceComponent implements OnInit {
  isUnAuthorized: boolean = false;
  insuranceId: string | null = null;
  insurers: Insurer[] = [];
  insurerEntry: string;
  insurerEntries: string[];

  selectedInsurance: Insurance = Insurance.New(
    this.insuranceService.defaultInsuranceType.PaymentPeriodMonths,
    this.insuranceService.defaultInsuranceType,
    this.insuranceService.defaultInsurancePolicy
  );

  title: string | null = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private insuranceService: InsuranceService,
    private insurerService: InsurerService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.insuranceId = params.get('id');
      if (this.insuranceId != null) {
        this.insuranceService.getInsurance(this.insuranceId).subscribe((x) => {
          this.selectedInsurance = new Insurance(
            x._id,
            x.PaymentPeriodMonths,
            x.PrevCollectionDate,
            this.formatDateString(x.PrevCollectionDate),
            x.NextCollectionDate,
            this.formatDateString(x.NextCollectionDate),
            x.Description,
            new Insurer(
              x._insurer._id,
              x._insurer.CompanyName,
              x._insurer.Address,
              x._insurer.Zipcode
            ),
            this.insuranceService.defaultInsuranceType,
            this.insuranceService.defaultInsurancePolicy
          );
          this.insurerEntry = this.selectedInsurance.Insurer.Name;
          this.title = 'Editing - ' + this.selectedInsurance.ID;
        });
      } else {
        this.title = 'New Insurance - ' + this.selectedInsurance.ID;
      }

      this.insurerService.getInsurers().subscribe((x) => {
        if (x.length > 0) {
          for (let i = 0; i < x.length; i++) {
            this.insurers.push(
              new Insurer(
                x[i]._id,
                x[i].CompanyName,
                x[i].Address,
                x[i].Zipcode
              )
            );
          }
          this.insurerEntries = this.insurers.map((x) => x.Name);
        }
      });
    });
  }

  async onSubmit() {
    this.selectedInsurance.Insurer = await this.insurerService.getInsurerByName(
      this.insurerEntry
    );
    if (this.insuranceId != null) {
      this.insuranceService
        .updateInsurance(
          this.insuranceId,
          this.selectedInsurance,
          this.insuranceService.defaultInsuranceType._id,
          this.insuranceService.defaultInsurancePolicy._id
        )
        .subscribe((x) => {
          if (x.status === 401) {
            this.isUnAuthorized = true;
          }
          if (x._id.trim().length > 0) {
            this.router.navigate(['insurances']);
          }
        });
    } else {
      this.insuranceService
        .createInsurance(
          this.selectedInsurance,
          this.insuranceService.defaultInsuranceType._id,
          this.insuranceService.defaultInsurancePolicy._id
        )
        .subscribe((x) => {
          if (x._id.trim().length > 0) {
            this.router.navigate(['insurances']);
          }
        });
    }
  }

  onDelete(): void {
    if (this.insuranceId) {
      this.insuranceService.deleteInsurance(this.insuranceId).subscribe((x) => {
        if (x.status === 401) {
          this.isUnAuthorized = true;
        }
        if (x._id.trim().length > 0) {
          this.router.navigate(['insurances']);
        }
      });
    }
  }

  formatDateString(date: Date): string {
    return formatDate(date, 'yyy-MM-dd', 'en-US');
  }
}
