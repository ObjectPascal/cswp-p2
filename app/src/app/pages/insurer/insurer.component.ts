import { Component, OnInit } from '@angular/core';
import { InsurerService } from 'src/app/services/insurer/insurer.service';
import { Insurer } from './insurer.model';

@Component({
  selector: 'app-insurer',
  templateUrl: './insurer.component.html',
  styleUrls: ['./insurer.component.css'],
})
export class InsurerComponent implements OnInit {
  insurers: Insurer[] = [];
  constructor(private insurerService: InsurerService) {}

  ngOnInit(): void {
    this.insurerService.getInsurers().subscribe((x) => {
      if (x.length > 0) {
        for (let i = 0; i < x.length; i++) {
          this.insurers.push(
            new Insurer(x[i]._id, x[i].CompanyName, x[i].Address, x[i].Zipcode)
          );
        }
      }
    });
  }
}
