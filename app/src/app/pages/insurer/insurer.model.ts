export class Insurer {
  ID: string;
  Name: string;
  Address: string;
  Zipcode: string;

  constructor(id: string, name: string, address: string, zipcode: string) {
    this.ID = id;
    this.Name = name;
    this.Address = address;
    this.Zipcode = zipcode;
  }

  static New(): Insurer {
    return new Insurer('', '', '', '');
  }
}
