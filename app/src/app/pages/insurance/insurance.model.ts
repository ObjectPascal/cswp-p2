import { formatDate } from '@angular/common';
import { Insurer } from '../insurer/insurer.model';

export class Insurance {
  ID: string;
  PaymentPeriodMonths: number;
  PrevCollectionDate: Date;
  PrevCollectionDateEntry: string;
  NextCollectionDate: Date;
  NextCollectionDateEntry: string;
  Description: string;
  Insurer: Insurer;
  InsuranceType: any;
  InsurancePolicy: any;

  constructor(
    id: string,
    PaymentPeriodMonths: number,
    PrevCollectionDate: Date,
    PrevCollectionDateEntry: string,
    NextCollectionDate: Date,
    NextCollectionDateEntry: string,
    Description: string,
    Insurer: Insurer,
    InsuranceType: any,
    InsurancePolicy: any
  ) {
    this.ID = id;
    this.PaymentPeriodMonths = PaymentPeriodMonths;
    this.PrevCollectionDate = PrevCollectionDate;
    this.PrevCollectionDateEntry = PrevCollectionDateEntry;
    this.NextCollectionDate = NextCollectionDate;
    this.NextCollectionDateEntry = NextCollectionDateEntry;
    this.Description = Description;
    this.Insurer = Insurer;
    this.InsuranceType = InsuranceType;
    this.InsurancePolicy = InsurancePolicy;
  }

  static New(
    defaultPaymentPeriodMonths: number,
    InsuranceType: any,
    InsurancePolicy: any
  ): Insurance {
    let currDate = new Date();
    return new Insurance(
      '',
      defaultPaymentPeriodMonths,
      currDate,
      formatDate(currDate, 'yyy-MM-dd', 'en-US'),
      currDate,
      formatDate(currDate, 'yyy-MM-dd', 'en-US'),
      '',
      Insurer.New(),
      InsuranceType,
      InsurancePolicy
    );
  }
}
