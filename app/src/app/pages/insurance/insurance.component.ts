import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { InsuranceService } from 'src/app/services/insurance/insurance.service';
import { Insurer } from '../insurer/insurer.model';
import { Insurance } from './insurance.model';

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.css'],
})
export class InsuranceComponent implements OnInit {
  insurances: Insurance[] = [];
  constructor(private insuranceService: InsuranceService) {}

  ngOnInit(): void {
    this.insuranceService.getInsurances().subscribe((x) => {
      if (x.length > 0) {
        for (let i = 0; i < x.length; i++) {
          this.insurances.push(
            new Insurance(
              x[i]._id,
              x[i].PaymentPeriodMonths,
              x[i].PrevCollectionDate,
              this.formatDateString(x[i].PrevCollectionDate),
              x[i].NextCollectionDate,
              this.formatDateString(x[i].NextCollectionDate),
              x[i].Description,
              new Insurer(
                x[i]._insurer._id,
                x[i]._insurer.CompanyName,
                x[i]._insurer.Address,
                x[i]._insurer.Zipcode
              ),
              this.insuranceService.defaultInsuranceType,
              this.insuranceService.defaultInsurancePolicy
            )
          );
        }
      }
    });
  }

  formatDateString(date: Date): string {
    return formatDate(date, 'yyy-MM-dd', 'en-US');
  }
}
