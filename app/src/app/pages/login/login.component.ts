import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  isFailedLogin: boolean = false;
  authForm: FormGroup;
  UserName: string;
  Password: string;
  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/');
    }
    this.route.paramMap.subscribe(
      (p) =>
        (this.authForm = new FormGroup({
          userName: new FormControl(this.UserName, [
            Validators.required,
            Validators.minLength(3),
          ]),
          password: new FormControl(this.Password, [
            Validators.required,
            Validators.minLength(1),
          ]),
        }))
    );
  }

  onSubmit(): void {
    if (this.UserName && this.Password) {
      this.authService
        .authenticate(this.UserName, this.Password)
        .subscribe((x) => {
          this.isFailedLogin = false;
          if (x.status === 404) {
            this.isFailedLogin = true;
          }
          if (x.token) {
            this.authService.setSession(x);
            this.router.navigateByUrl('/');
          }
        });
    }
  }
}
