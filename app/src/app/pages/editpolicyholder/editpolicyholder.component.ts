import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PolicyHolderService } from 'src/app/services/policyholder/policyholder.service';
import { PolicyHolder } from '../policyholder/policyholder.model';

@Component({
  selector: 'app-editpolicyholder',
  templateUrl: './editpolicyholder.component.html',
  styleUrls: ['./editpolicyholder.component.css'],
})
export class EditPolicyHolderComponent implements OnInit {
  isUnAuthorized: boolean = false;
  policyHolderId: string | null = null;
  selectedPolicyHolder: PolicyHolder = PolicyHolder.New();

  title: string | null = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private policyHolderService: PolicyHolderService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.policyHolderId = params.get('id');
      if (this.policyHolderId != null) {
        this.policyHolderService
          .getPolicyHolder(this.policyHolderId)
          .subscribe((x) => {
            this.selectedPolicyHolder = new PolicyHolder(
              x._id,
              x.IBAN,
              x.FullName,
              x.Initials,
              x.Email,
              x.Telephone,
              x.Address,
              x.Zipcode,
              x.Lat,
              x.Long
            );
            console.log(this.selectedPolicyHolder);
            this.title =
              'Editing - ' +
              this.selectedPolicyHolder.FullName +
              ' - ' +
              this.selectedPolicyHolder.ID;
          });
      } else {
        this.title = 'New PolicyHolder - ' + this.selectedPolicyHolder.ID;
      }
    });
  }

  onSubmit(): void {
    if (this.policyHolderId != null) {
      this.policyHolderService
        .updatePolicyHolder(this.policyHolderId, this.selectedPolicyHolder)
        .subscribe((x) => {
          this.isUnAuthorized = false;
          if (x.status === 401) {
            this.isUnAuthorized = true;
          }
          if (x._id.trim().length > 0) {
            this.router.navigate(['policyholders']);
          }
        });
    } else {
      this.policyHolderService
        .createPolicyHolder(this.selectedPolicyHolder)
        .subscribe((x) => {
          if (x._id.trim().length > 0) {
            this.router.navigate(['policyholders']);
          }
        });
    }
  }

  onDelete(): void {
    if (this.policyHolderId != null) {
      this.policyHolderService
        .deletePolicyHolder(this.policyHolderId)
        .subscribe((x) => {
          this.isUnAuthorized = false;
          if (x.status === 401) {
            this.isUnAuthorized = true;
          }
          if (x._id.trim().length > 0) {
            this.router.navigate(['policyholders']);
          }
        });
    }
  }
}
