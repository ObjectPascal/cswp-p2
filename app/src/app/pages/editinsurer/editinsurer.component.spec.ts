import { HttpClient } from '@angular/common/http';
import { Directive, HostListener, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { InsurerService } from 'src/app/services/insurer/insurer.service';
import { InsurerComponent } from '../insurer/insurer.component';
import { Insurer } from '../insurer/insurer.model';

import { EditinsurerComponent } from './editinsurer.component';

@Directive({
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onclick(): void {
    this.navigatedTo = this.linkParams;
  }
}

const expectedInsurer: Insurer = {
  ID: '61afb602f80d111971f30148',
  Name: 'Mock',
  Address: 'Mock',
  Zipcode: 'Mock',
};

const expectedInsurerResponse = {
  status: 200,
  _id: '61afb602f80d111971f30148',
  FullName: 'Mock',
  CompanyName: 'Mock',
  Address: 'Mock',
  Zipcode: 'Mock',
  _entityData: {
    _changedById: 0,
    LastChanged: new Date(),
  },
};

describe('EditinsurerComponent', () => {
  let component: EditinsurerComponent;
  let fixture: ComponentFixture<EditinsurerComponent>;

  let httpSpy: jasmine.SpyObj<HttpClient>;
  let insurerServiceSpy: jasmine.SpyObj<InsurerService>;

  beforeEach(async () => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['']);
    insurerServiceSpy = jasmine.createSpyObj('InsurerService', [
      'getInsurer',
      'getInsurers',
      'updateInsurer',
      'createInsurer',
      'deleteInsurer',
    ]);

    await TestBed.configureTestingModule({
      declarations: [EditinsurerComponent, RouterLinkStubDirective],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ id: expectedInsurer.ID })),
          },
        },
        { provide: HttpClient, useValue: httpSpy },
        { provide: InsurerService, useValue: insurerServiceSpy },
      ],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'insurers', component: InsurerComponent },
        ]),
      ],
    }).compileComponents();

    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
    insurerServiceSpy = TestBed.inject(
      InsurerService
    ) as jasmine.SpyObj<InsurerService>;

    fixture = TestBed.createComponent(EditinsurerComponent);
    component = fixture.componentInstance;
    component.isUnAuthorized = false;
    component.insurerId = expectedInsurer.ID;
    component.selectedInsurer = expectedInsurer;
  });

  beforeEach(() => {
    fixture.destroy();
  });

  it('component should be truthy', (done: DoneFn) => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    done();
  });

  it('#ngOnInit should call getInsurer', (done: DoneFn) => {
    insurerServiceSpy.getInsurer.and.returnValue(of(expectedInsurerResponse));

    fixture.detectChanges();
    component.ngOnInit();

    expect(insurerServiceSpy.getInsurer.calls.count()).toEqual(1);
    done();
  });

  it('#onSubmit with insurerId should call updateUser', (done: DoneFn) => {
    insurerServiceSpy.updateInsurer.and.returnValue(
      of(expectedInsurerResponse)
    );

    fixture.detectChanges();
    component.insurerId = expectedInsurer.ID;
    component.onSubmit();

    expect(insurerServiceSpy.updateInsurer.calls.count()).toEqual(1);
    done();
  });

  it('#onSubmit without insurerId should call createUser', (done: DoneFn) => {
    insurerServiceSpy.createInsurer.and.returnValue(
      of(expectedInsurerResponse)
    );

    fixture.detectChanges();
    component.insurerId = null;
    component.onSubmit();

    expect(insurerServiceSpy.createInsurer.calls.count()).toEqual(1);
    done();
  });

  it('#onDelete should call delete', (done: DoneFn) => {
    insurerServiceSpy.deleteInsurer.and.returnValue(
      of(expectedInsurerResponse)
    );

    fixture.detectChanges();
    component.onDelete();

    expect(insurerServiceSpy.deleteInsurer.calls.count()).toEqual(1);
    done();
  });

  it('service.getInsurer should get correct Insurer', (done: DoneFn) => {
    insurerServiceSpy.getInsurer.and.returnValue(of(expectedInsurerResponse));

    fixture.detectChanges();
    component.ngOnInit();

    expect(component.selectedInsurer).toBeTruthy();
    expect(component.selectedInsurer.ID).toEqual(expectedInsurer.ID);
    expect(component.selectedInsurer.Name).toEqual(expectedInsurer.Name);
    expect(component.selectedInsurer.Address).toEqual(expectedInsurer.Address);
    expect(component.selectedInsurer.Zipcode).toEqual(expectedInsurer.Zipcode);
    done();
  });

  it('service.updateInsurer should get correct Insurer', (done: DoneFn) => {
    insurerServiceSpy.updateInsurer.and.returnValue(
      of(expectedInsurerResponse)
    );

    fixture.detectChanges();
    component.onSubmit();

    expect(component.selectedInsurer).toBeTruthy();
    expect(component.selectedInsurer.ID).toEqual(expectedInsurer.ID);
    expect(component.selectedInsurer.Name).toEqual(expectedInsurer.Name);
    expect(component.selectedInsurer.Address).toEqual(expectedInsurer.Address);
    expect(component.selectedInsurer.Zipcode).toEqual(expectedInsurer.Zipcode);
    done();
  });

  it('service.createInsurer should get correct Insurer', (done: DoneFn) => {
    insurerServiceSpy.createInsurer.and.returnValue(
      of(expectedInsurerResponse)
    );

    fixture.detectChanges();
    component.insurerId = null;
    component.onSubmit();

    expect(component.selectedInsurer).toBeTruthy();
    expect(component.selectedInsurer.ID).toEqual(expectedInsurer.ID);
    expect(component.selectedInsurer.Name).toEqual(expectedInsurer.Name);
    expect(component.selectedInsurer.Address).toEqual(expectedInsurer.Address);
    expect(component.selectedInsurer.Zipcode).toEqual(expectedInsurer.Zipcode);
    done();
  });

  it('service.deleteInsurer should get correct Insurer', (done: DoneFn) => {
    insurerServiceSpy.deleteInsurer.and.returnValue(
      of(expectedInsurerResponse)
    );

    fixture.detectChanges();
    component.onDelete();

    expect(component.selectedInsurer).toBeTruthy();
    expect(component.selectedInsurer.ID).toEqual(expectedInsurer.ID);
    expect(component.selectedInsurer.Name).toEqual(expectedInsurer.Name);
    expect(component.selectedInsurer.Address).toEqual(expectedInsurer.Address);
    expect(component.selectedInsurer.Zipcode).toEqual(expectedInsurer.Zipcode);
    done();
  });
});
