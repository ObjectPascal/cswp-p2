import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InsurerService } from 'src/app/services/insurer/insurer.service';
import { Insurer } from '../insurer/insurer.model';

@Component({
  selector: 'app-editinsurer',
  templateUrl: './editinsurer.component.html',
  styleUrls: ['./editinsurer.component.css'],
})
export class EditinsurerComponent implements OnInit {
  isUnAuthorized: boolean = false;
  insurerId: string | null = null;
  selectedInsurer: Insurer = Insurer.New();

  title: string | null = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private insurerService: InsurerService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.insurerId = params.get('id');
      if (this.insurerId != null) {
        this.insurerService.getInsurer(this.insurerId).subscribe((x) => {
          this.selectedInsurer = new Insurer(
            x._id,
            x.CompanyName,
            x.Address,
            x.Zipcode
          );
          this.title =
            'Editing - ' +
            this.selectedInsurer.Name +
            ' - ' +
            this.selectedInsurer.ID;
        });
      } else {
        this.title = 'New Insurer - ' + this.selectedInsurer.ID;
      }
    });
  }

  onSubmit(): void {
    if (this.insurerId != null) {
      this.insurerService
        .updateInsurer(this.insurerId, this.selectedInsurer)
        .subscribe((x) => {
          this.isUnAuthorized = false;
          if (x.status === 401) {
            this.isUnAuthorized = true;
          }
          if (x._id.trim().length > 0) {
            this.router.navigate(['insurers']);
          }
        });
    } else {
      this.insurerService.createInsurer(this.selectedInsurer).subscribe((x) => {
        if (x._id.trim().length > 0) {
          this.router.navigate(['insurers']);
        }
      });
    }
  }

  onDelete(): void {
    if (this.insurerId) {
      this.insurerService.deleteInsurer(this.insurerId).subscribe((x) => {
        this.isUnAuthorized = false;
        if (x.status === 401) {
          this.isUnAuthorized = true;
        }
        if (x._id.trim().length > 0) {
          this.router.navigate(['insurers']);
        }
      });
    }
  }
}
