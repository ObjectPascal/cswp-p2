import { formatDate } from '@angular/common';
import { Insurer } from '../insurer/insurer.model';

export class User {
  ID: number;
  UserName: string;
  Initials: string;
  DateOfBirth: Date;
  DateOfBirthEntry: string;
  Gender: string;
  Insurer: Insurer;

  constructor(
    id: number,
    userName: string,
    initials: string,
    dateOfBirth: Date,
    dateOfBirthEntry: string,
    gender: string,
    insurer: Insurer
  ) {
    this.ID = id;
    this.UserName = userName;
    this.Initials = initials;
    this.DateOfBirth = dateOfBirth;
    this.DateOfBirthEntry = dateOfBirthEntry;
    this.Gender = gender;
    this.Insurer = insurer;
  }

  static New(): User {
    let currDate = new Date();
    return new User(
      -1,
      '',
      '',
      currDate,
      'None',
      formatDate(currDate, 'yyy-MM-dd', 'en-US'),
      Insurer.New()
    );
  }

  static Default(): User {
    let currDate = new Date();
    return new User(
      -1,
      '',
      '',
      new Date(),
      'None',
      formatDate(currDate, 'yyy-MM-dd', 'en-US'),
      Insurer.New()
    );
  }
}
