import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/pages/user/user.model';
import { InsurerService } from 'src/app/services/insurer/insurer.service';
import { InsurerResponse } from 'src/app/services/insurer/InsurerResponse';
import { UserService } from 'src/app/services/user/user.service';
import { Insurer } from '../insurer/insurer.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  users: User[] = [];

  constructor(
    private userService: UserService,
    private insurerService: InsurerService
  ) {}

  ngOnInit(): void {
    this.userService.getUsers().subscribe(async (x) => {
      if (x.length > 0) {
        for (let i = 0; i < x.length; i++) {
          let date = new Date(
            x[i].DateOfBirth.year.low,
            x[i].DateOfBirth.month.low,
            x[i].DateOfBirth.day.low
          );

          let insurer: Insurer = await this.insurerService.getInsurerAsync(
            x[i].Data._insurerId
          );

          this.users.push(
            new User(
              x[i].id,
              x[i].UserName,
              x[i].Initials,
              date,
              this.formatDateString(date),
              x[i].Gender,
              insurer
            )
          );
        }
      }
    });
  }

  formatDateString(date: Date): string {
    return formatDate(date, 'yyy-MM-dd', 'en-US');
  }
}
