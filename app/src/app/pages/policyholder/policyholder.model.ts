export class PolicyHolder {
  ID: string;
  IBAN: string;
  FullName: string;
  Initials: string;
  Email: string;
  Telephone: string;
  Address: string;
  Zipcode: string;
  Lat: number;
  Long: number;

  constructor(
    id: string,
    IBAN: string,
    FullName: string,
    Initials: string,
    Email: string,
    Telephone: string,
    Address: string,
    Zipcode: string,
    Lat: number,
    Long: number
  ) {
    this.ID = id;
    this.IBAN = IBAN;
    this.FullName = FullName;
    this.Initials = Initials;
    this.Email = Email;
    this.Telephone = Telephone;
    this.Address = Address;
    this.Zipcode = Zipcode;
    this.Lat = Lat;
    this.Long = Long;
  }

  static New(): PolicyHolder {
    return new PolicyHolder('', '', '', '', '', '', '', '', 0, 0);
  }
}
