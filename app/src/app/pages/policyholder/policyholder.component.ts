import { Component, OnInit } from '@angular/core';
import { PolicyHolderService } from 'src/app/services/policyholder/policyholder.service';
import { PolicyHolder } from './policyholder.model';

@Component({
  selector: 'app-policyholder',
  templateUrl: './policyholder.component.html',
  styleUrls: ['./policyholder.component.css'],
})
export class PolicyholderComponent implements OnInit {
  policyHolders: PolicyHolder[] = [];
  constructor(private policyHolderService: PolicyHolderService) {}

  ngOnInit(): void {
    this.policyHolderService.getPolicyHolders().subscribe((x) => {
      if (x.length > 0) {
        for (let i = 0; i < x.length; i++) {
          this.policyHolders.push(
            new PolicyHolder(
              x[i]._id,
              x[i].IBAN,
              x[i].FullName,
              x[i].Initials,
              x[i].Email,
              x[i].Telephone,
              x[i].Address,
              x[i].Zipcode,
              x[i].Lat,
              x[i].Long
            )
          );
        }
      }
    });
  }
}
