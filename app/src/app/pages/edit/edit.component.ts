import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InsurerService } from 'src/app/services/insurer/insurer.service';
import { UserService } from 'src/app/services/user/user.service';
import { Insurer } from '../insurer/insurer.model';
import { User } from '../user/user.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  userForm: FormGroup;
  userId: string | null = null;
  selectedUser: User = User.Default();

  title: string | null = '';
  genders: string[] = ['Male', 'Female'];
  insurers: Insurer[] = [];
  insurerEntry: string;
  insurerEntries: string[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private insurerService: InsurerService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((p) => {
      this.userId = p.get('id');
      if (this.userId !== null) {
        this.userService.getUser(Number(this.userId)).subscribe((x) => {
          if (x.id || x.id > -1) {
            let date = new Date(
              x.DateOfBirth.year.low,
              x.DateOfBirth.month.low,
              x.DateOfBirth.day.low
            );
            this.selectedUser = new User(
              x.id,
              x.UserName,
              x.Initials,
              date,
              this.formatDateString(date),
              x.Gender,
              new Insurer(
                x.Data._insurerId,
                x.Data.CompanyName,
                x.Data.ZipCode,
                x.Data.ZipCode
              )
            );
            this.insurerEntry = this.selectedUser.Insurer.Name;

            this.title =
              'Editing - ' +
              this.selectedUser.UserName +
              ' - ' +
              this.selectedUser.ID;
          }
        });
      } else {
        this.selectedUser = User.New();
        this.title = 'New User';
      }

      this.insurerService.getInsurers().subscribe((x) => {
        if (x.length > 0) {
          for (let i = 0; i < x.length; i++) {
            this.insurers.push(
              new Insurer(
                x[i]._id,
                x[i].CompanyName,
                x[i].Address,
                x[i].Zipcode
              )
            );
          }
          this.insurerEntries = this.insurers.map((x) => x.Name);
        }
      });

      this.userForm = new FormGroup({
        userName: new FormControl(this.selectedUser.UserName, [
          Validators.required,
          Validators.minLength(3),
        ]),
        initials: new FormControl(this.selectedUser.Initials, [
          Validators.required,
          Validators.minLength(1),
        ]),
        insurer: new FormControl(this.selectedUser.Insurer, [
          Validators.required,
        ]),
        dateOfBirth: new FormControl(this.selectedUser.DateOfBirth, [
          Validators.required,
        ]),
        gender: new FormControl(this.selectedUser.Gender, [
          Validators.required,
        ]),
      });
    });
  }

  async onSubmit() {
    this.selectedUser.Insurer = await this.insurerService.getInsurerByName(
      this.insurerEntry
    );
    if (this.userId !== null) {
      this.userService
        .updateUser(Number(this.userId), this.selectedUser)
        .subscribe((x) => {
          if (x.id > -1) {
            this.router.navigate(['users']).catch((e) => {
              console.log('Route users not found');
            });
          }
        });
    } else {
      this.userService.createUser(this.selectedUser).subscribe((x) => {
        if (x.id > -1) {
          this.router.navigate(['users']).catch((e) => {
            console.log('Route users not found');
          });
        }
      });
    }
  }

  onDelete(): void {
    if (this.userId) {
      this.userService.deleteUser(Number(this.userId)).subscribe((x) => {
        if (x.id > -1) {
          this.router.navigate(['users']).catch((e) => {
            console.log('Route users not found');
          });
        }
      });
    }
  }

  formatDateString(date: Date): string {
    return formatDate(date, 'yyy-MM-dd', 'en-US');
  }
}
