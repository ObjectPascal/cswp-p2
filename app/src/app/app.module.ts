import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserComponent } from './pages/user/user.component';
import { NavComponent } from './shared/nav/nav.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { EditComponent } from './pages/edit/edit.component';
import { AboutComponent } from './pages/about/about.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InsurerComponent } from './pages/insurer/insurer.component';
import { EditinsurerComponent } from './pages/editinsurer/editinsurer.component';
import { LoginComponent } from './pages/login/login.component';
import { PolicyholderComponent } from './pages/policyholder/policyholder.component';
import { EditPolicyHolderComponent } from './pages/editpolicyholder/editpolicyholder.component';
import { InsuranceComponent } from './pages/insurance/insurance.component';
import { EditinsuranceComponent } from './pages/editinsurance/editinsurance.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    EditComponent,
    AboutComponent,
    InsurerComponent,
    EditinsurerComponent,
    LoginComponent,
    PolicyholderComponent,
    EditPolicyHolderComponent,
    InsuranceComponent,
    EditinsuranceComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
