export interface PolicyHolderResponse {
  status: number;
  _id: string;
  IBAN: string;
  FullName: string;
  Initials: string;
  Email: string;
  Telephone: string;
  Address: string;
  Zipcode: string;
  Lat: number;
  Long: number;
  _entityData: {
    _changedById: number;
    LastChanged: Date;
  };
}
