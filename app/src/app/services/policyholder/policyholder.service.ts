import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, of, throwError } from 'rxjs';
import { PolicyHolder } from 'src/app/pages/policyholder/policyholder.model';
import { AuthService } from '../auth/auth.service';
import { PolicyHolderResponse } from './PolicyHolderResponse';

@Injectable({
  providedIn: 'root',
})
export class PolicyHolderService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.log(error);
      console.error(`An error ocurred: ${error.error}`);
    } else {
      console.error(`An error ocurred: ${error.error}`);
    }
    throwError(() => new Error('PolicyHolderService error'));
  }

  getPolicyHolders() {
    return this.http
      .get<PolicyHolderResponse[]>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/policyholders/list',
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of([
            {
              status: e.status,
              _id: '',
              IBAN: '',
              FullName: '',
              Initials: '',
              Email: '',
              Telephone: '',
              Address: '',
              Zipcode: '',
              Lat: -1,
              Long: -1,
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
          ]);
        })
      );
  }

  getPolicyHolder(id: string) {
    return this.http
      .get<PolicyHolderResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/policyholders/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            IBAN: '',
            FullName: '',
            Initials: '',
            Email: '',
            Telephone: '',
            Address: '',
            Zipcode: '',
            Lat: -1,
            Long: -1,
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  createPolicyHolder(policyHolder: PolicyHolder) {
    let data = {
      IBAN: policyHolder.IBAN,
      FullName: policyHolder.FullName,
      Initials: policyHolder.Initials,
      Email: policyHolder.Email,
      Telephone: policyHolder.Telephone,
      Address: policyHolder.Address,
      Zipcode: policyHolder.Zipcode,
      Lat: policyHolder.Lat,
      Long: policyHolder.Long,
    };
    return this.http
      .post<PolicyHolderResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/policyholders/',
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            IBAN: '',
            FullName: '',
            Initials: '',
            Email: '',
            Telephone: '',
            Address: '',
            Zipcode: '',
            Lat: -1,
            Long: -1,
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  updatePolicyHolder(id: string, policyHolder: PolicyHolder) {
    let data = {
      IBAN: policyHolder.IBAN,
      FullName: policyHolder.FullName,
      Initials: policyHolder.Initials,
      Email: policyHolder.Email,
      Telephone: policyHolder.Telephone,
      Address: policyHolder.Address,
      Zipcode: policyHolder.Zipcode,
      Lat: policyHolder.Lat,
      Long: policyHolder.Long,
    };
    return this.http
      .put<PolicyHolderResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/policyholders/' + id,
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            IBAN: '',
            FullName: '',
            Initials: '',
            Email: '',
            Telephone: '',
            Address: '',
            Zipcode: '',
            Lat: -1,
            Long: -1,
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  deletePolicyHolder(id: string) {
    return this.http
      .delete<PolicyHolderResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/policyholders/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            IBAN: '',
            FullName: '',
            Initials: '',
            Email: '',
            Telephone: '',
            Address: '',
            Zipcode: '',
            Lat: -1,
            Long: -1,
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }
}
