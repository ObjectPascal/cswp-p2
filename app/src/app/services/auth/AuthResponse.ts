export interface AuthResponse {
  status: number;
  token: string;
  issued: number;
  expires: number;
}
