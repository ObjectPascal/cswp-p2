import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { catchError, of, throwError } from 'rxjs';
import { AuthResponse } from './AuthResponse';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _headers = {
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Methods': 'POST',
    'Access-Control-Allow-Origin': '*',
  };

  private _requestOptions = {
    headers: new HttpHeaders(this._headers),
  };

  constructor(private http: HttpClient, private router: Router) {}

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error(`An error ocurred: ${error.error}`);
    } else {
      console.error(`An error ocurred: ${error.error}`);
    }
    throwError(() => new Error('AuthService error'));
  }

  authenticate(userName: string, password: string) {
    return this.http
      .post<AuthResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/auth/',
        {
          UserName: userName,
          PasswordHash: password,
        },
        this._requestOptions
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            token: '',
            issued: 0,
            expires: 0,
          });
        })
      );
  }

  getSession(): string {
    return localStorage.getItem('auth_token');
  }

  setSession(authResponse: AuthResponse) {
    localStorage.setItem('auth_token', authResponse.token);
  }

  logout() {
    localStorage.removeItem('auth_token');
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem('auth_token')) {
      return true;
    }
    return false;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.isLoggedIn()) {
      return true;
    }
    this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
