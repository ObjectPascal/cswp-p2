import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, firstValueFrom, of, throwError } from 'rxjs';
import { Insurer } from 'src/app/pages/insurer/insurer.model';
import { AuthService } from '../auth/auth.service';
import { InsurerResponse } from './InsurerResponse';

@Injectable({
  providedIn: 'root',
})
export class InsurerService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error(`An error ocurred: ${error.error}`);
    } else {
      console.error(`An error ocurred: ${error.error}`);
    }
  }

  getInsurers() {
    return this.http
      .get<InsurerResponse[]>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurers/list',
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of([
            {
              status: e.status,
              _id: '',
              FullName: '',
              CompanyName: '',
              Address: '',
              Zipcode: '',
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
          ]);
        })
      );
  }

  getInsurer(id: string) {
    return this.http
      .get<InsurerResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurers/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            FullName: '',
            CompanyName: '',
            Address: '',
            Zipcode: '',
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  async getInsurerAsync(id: string): Promise<Insurer> {
    let insurer: InsurerResponse = await firstValueFrom(this.getInsurer(id));
    return new Insurer(
      insurer._id,
      insurer.CompanyName,
      insurer.Address,
      insurer.Zipcode
    );
  }

  async getInsurerByName(name: string): Promise<Insurer> {
    let insurers: InsurerResponse[] = await firstValueFrom(this.getInsurers());
    let insurer = insurers.filter((x) => x.CompanyName === name)[0];
    return new Insurer(
      insurer._id,
      insurer.CompanyName,
      insurer.Address,
      insurer.Zipcode
    );
  }

  createInsurer(insurer: Insurer) {
    let data = {
      FullName: insurer.Name + ' User',
      CompanyName: insurer.Name,
      Address: insurer.Address,
      Zipcode: insurer.Zipcode,
    };
    return this.http
      .post<InsurerResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurers/',
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            FullName: '',
            CompanyName: '',
            Address: '',
            Zipcode: '',
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  updateInsurer(id: string, insurer: Insurer) {
    let data = {
      FullName: insurer.Name + ' User',
      CompanyName: insurer.Name,
      Address: insurer.Address,
      Zipcode: insurer.Zipcode,
    };
    return this.http
      .put<InsurerResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurers/' + id,
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            FullName: '',
            CompanyName: '',
            Address: '',
            Zipcode: '',
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  deleteInsurer(id: string) {
    return this.http
      .delete<InsurerResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurers/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            FullName: '',
            CompanyName: '',
            Address: '',
            Zipcode: '',
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }
}
