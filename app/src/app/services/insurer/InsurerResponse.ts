export interface InsurerResponse {
  status: number;
  _id: string;
  FullName: string;
  CompanyName: string;
  Address: string;
  Zipcode: string;
  _entityData: {
    _changedById: number;
    LastChanged: Date;
  };
}
