import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Insurer } from 'src/app/pages/insurer/insurer.model';

import { InsurerService } from './insurer.service';

let insurerPostData: Insurer = {
  ID: '-',
  Name: 'Mock',
  Address: 'Mock',
  Zipcode: 'Mock',
};

let insurerUpdateData: Insurer = {
  ID: '-',
  Name: 'Mock 2',
  Address: 'Mock 2',
  Zipcode: 'Mock 2',
};

const expectedInsurerUpdate = {
  status: 200,
  _id: '61afb602f80d111971f30148',
  FullName: 'Mock 2',
  CompanyName: 'Mock 2',
  Address: 'Mock 2',
  Zipcode: 'Mock 2',
  _entityData: {
    _changedById: 0,
    LastChanged: new Date(),
  },
};

const expectedInsurer = {
  status: 200,
  _id: '61afb602f80d111971f30148',
  FullName: 'Mock',
  CompanyName: 'Mock',
  Address: 'Mock',
  Zipcode: 'Mock',
  _entityData: {
    _changedById: 0,
    LastChanged: new Date(),
  },
};

const expectedInsurers = [
  {
    status: 200,
    _id: '61afb602f80d111971f30148',
    FullName: 'Mock',
    CompanyName: 'Mock',
    Address: 'Mock',
    Zipcode: 'Mock',
    _entityData: {
      _changedById: 0,
      LastChanged: new Date(),
    },
  },
];

describe('InsurerService', () => {
  let service: InsurerService;
  let httpSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', [
      'pipe',
      'get',
      'post',
      'put',
      'delete',
    ]);

    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpSpy }],
    });
    service = TestBed.inject(InsurerService);
    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('service should be truthy', () => {
    expect(service).toBeTruthy();
  });

  it('#getInsurer should return an Insurer', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedInsurer));

    service.getInsurer(expectedInsurer._id).subscribe((x) => {
      expect(x.status).toEqual(200);
      expect(x._id).toEqual(expectedInsurer._id);
      expect(x.FullName).toEqual(expectedInsurer.FullName);
      expect(x.CompanyName).toEqual(expectedInsurer.CompanyName);
      expect(x.Address).toEqual(expectedInsurer.Address);
      expect(x.Zipcode).toEqual(expectedInsurer.Zipcode);
      done();
    });
  });

  it('#getInsurers should return a list of Insurers', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedInsurers));

    service.getInsurers().subscribe((x) => {
      expect(x.length).toBe(1);
      expect(x[0].status).toEqual(200);
      expect(x[0]._id).toEqual(expectedInsurers[0]._id);
      expect(x[0].FullName).toEqual(expectedInsurers[0].FullName);
      expect(x[0].CompanyName).toEqual(expectedInsurers[0].CompanyName);
      expect(x[0].Address).toEqual(expectedInsurers[0].Address);
      expect(x[0].Zipcode).toEqual(expectedInsurers[0].Zipcode);
      done();
    });
  });

  it('#createInsurer should return the created Insurer', (done: DoneFn) => {
    httpSpy.post.and.returnValue(of(expectedInsurer));

    service.createInsurer(insurerPostData).subscribe((x) => {
      expect(x.status).toEqual(200);

      expect(x._id).toEqual(expectedInsurer._id);
      expect(x.FullName).toEqual(insurerPostData.Name);
      expect(x.CompanyName).toEqual(insurerPostData.Name);
      expect(x.Address).toEqual(insurerPostData.Address);
      expect(x.Zipcode).toEqual(insurerPostData.Zipcode);
      done();
    });
  });

  it('#updateInsurer should return the updated Insurer', (done: DoneFn) => {
    httpSpy.put.and.returnValue(of(expectedInsurerUpdate));

    service
      .updateInsurer(expectedInsurerUpdate._id, insurerUpdateData)
      .subscribe((x) => {
        expect(x.status).toEqual(200);

        expect(x._id).toEqual(expectedInsurerUpdate._id);
        expect(x.FullName).toEqual(insurerUpdateData.Name);
        expect(x.CompanyName).toEqual(insurerUpdateData.Name);
        expect(x.Address).toEqual(insurerUpdateData.Address);
        expect(x.Zipcode).toEqual(insurerUpdateData.Zipcode);
        done();
      });
  });

  it('#deleteInsurer should return the deleted Insurer', (done: DoneFn) => {
    httpSpy.delete.and.returnValue(of(expectedInsurer));

    service.deleteInsurer(expectedInsurer._id).subscribe((x) => {
      expect(x.status).toEqual(200);

      expect(x._id).toEqual(expectedInsurer._id);
      expect(x.FullName).toEqual(expectedInsurer.FullName);
      expect(x.CompanyName).toEqual(expectedInsurer.CompanyName);
      expect(x.Address).toEqual(expectedInsurer.Address);
      expect(x.Zipcode).toEqual(expectedInsurer.Zipcode);
      done();
    });
  });
});
