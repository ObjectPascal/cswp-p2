export interface UserResponse {
  status: number;
  id: number;
  UserName: string;
  Initials: string;
  Gender: string;
  DateOfBirth: {
    year: {
      low: number;
    };
    month: {
      low: number;
    };
    day: {
      low: number;
    };
  };
  Data: {
    _insurerId: string;
    CompanyName: string;
    ZipCode: string;
  };
}

export interface UserDeleteResponse {
  id: number;
}
