import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { User } from 'src/app/pages/user/user.model';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { AuthResponse } from '../auth/AuthResponse';
import { catchError, Observable, of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { UserDeleteResponse, UserResponse } from './UserResponse';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  users: User[] = [];

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.log(error);
      console.error(`An error ocurred: ${error.error}`);
    } else {
      console.error(`An error ocurred: ${error.error}`);
    }
    throwError(() => new Error('UserService error'));
  }

  getUser(id: number) {
    return this.http
      .get<UserResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/users/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            id: -1,
            UserName: '',
            Initials: '',
            Gender: '',
            DateOfBirth: {
              year: {
                low: 0,
              },
              month: {
                low: 0,
              },
              day: {
                low: 0,
              },
            },
            Data: {
              _insurerId: '',
              CompanyName: '',
              ZipCode: '',
            },
          });
        })
      );
  }

  getUsers() {
    return this.http
      .get<UserResponse[]>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/users/list',
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of([
            {
              status: e.status,
              id: -1,
              UserName: '',
              Initials: '',
              Gender: '',
              DateOfBirth: {
                year: {
                  low: 0,
                },
                month: {
                  low: 0,
                },
                day: {
                  low: 0,
                },
              },
              Data: {
                _insurerId: '',
                CompanyName: '',
                ZipCode: '',
              },
            },
          ]);
        })
      );
  }

  createUser(user: User) {
    let data = {
      UserName: user.UserName,
      Initials: user.Initials,
      Gender: user.Gender,
      DateOfBirth: this.formatDateString(
        new Date(Date.parse(user.DateOfBirthEntry))
      ),
      PasswordHash: 'dummypassword',
      Data: {
        _insurerId: user.Insurer.ID,
        CompanyName: user.Insurer.Name,
        ZipCode: user.Insurer.Zipcode,
      },
    };
    console.log(data);
    return this.http
      .post<UserResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/users',
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            id: -1,
            UserName: '',
            Initials: '',
            Gender: '',
            DateOfBirth: {
              year: {
                low: 0,
              },
              month: {
                low: 0,
              },
              day: {
                low: 0,
              },
            },
            Data: {
              _insurerId: '',
              CompanyName: '',
              ZipCode: '',
            },
          });
        })
      );
  }

  updateUser(id: number, user: User) {
    let data = {
      UserName: user.UserName,
      Initials: user.Initials,
      Gender: user.Gender,
      DateOfBirth: this.formatDateString(
        new Date(Date.parse(user.DateOfBirthEntry))
      ),
      PasswordHash: 'dummypassword',
      Data: {
        _insurerId: user.Insurer.ID,
        CompanyName: user.Insurer.Name,
        ZipCode: user.Insurer.Zipcode,
      },
    };
    return this.http
      .put<UserResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/users/' + id,
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            id: -1,
            UserName: '',
            Initials: '',
            Gender: '',
            DateOfBirth: {
              year: {
                low: 0,
              },
              month: {
                low: 0,
              },
              day: {
                low: 0,
              },
            },
            Data: {
              _insurerId: '',
              CompanyName: '',
              ZipCode: '',
            },
          });
        })
      );
  }

  deleteUser(id: number) {
    return this.http
      .delete<UserDeleteResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/users/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            id: -1,
          });
        })
      );
  }

  formatDateString(date: Date): string {
    return formatDate(date, 'yyy-MM-ddTH:mm:ss', 'en-US');
  }
}
