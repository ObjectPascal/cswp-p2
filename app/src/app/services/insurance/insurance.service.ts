import { formatDate } from '@angular/common';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, of, throwError } from 'rxjs';
import { Insurance } from 'src/app/pages/insurance/insurance.model';
import { Insurer } from 'src/app/pages/insurer/insurer.model';
import { AuthService } from '../auth/auth.service';
import { InsuranceResponse } from './insuranceResponse';

@Injectable({
  providedIn: 'root',
})
export class InsuranceService {
  defaultInsuranceType = {
    _id: '61afa6550e3a53abe08a73fd',
    TypeName: 'Verzekering Basis',
    PaymentPeriodMonths: 3,
    _entityData: {
      _changedById: 1,
      LastChanged: new Date(2021, 12, 7, 18, 22, 13),
    },
  };
  defaultInsurancePolicy = {
    _id: '61afa6550e3a53abe08a73fd',
    _policyHolder: '61af5edcecac3e19058f2879',
    PolicyNumber: 123,
    Premium: 100.95,
    DiscountPercentage: 5,
    _entityData: {
      _changedById: 1,
      LastChanged: new Date(2021, 12, 7, 14, 54, 11),
    },
  };

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.log(error);
      console.error(`An error ocurred: ${error.error}`);
    } else {
      console.error(`An error ocurred: ${error.error}`);
    }
    throwError(() => new Error('InsuranceService error'));
  }

  getInsurances() {
    return this.http
      .get<InsuranceResponse[]>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurances/list',
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of([
            {
              status: e.status,
              _id: '',
              PaymentPeriodMonths: -1,
              PrevCollectionDate: null,
              NextCollectionDate: null,
              Description: '',
              _insurer: {
                _id: '',
                FullName: '',
                CompanyName: '',
                Address: '',
                Zipcode: '',
                _entityData: {
                  _changedById: -1,
                  LastChanged: null,
                },
              },
              _insuranceType: {
                _id: '',
                TypeName: '',
                PaymentPeriodMonths: -1,
                _entityData: {
                  _changedById: -1,
                  LastChanged: null,
                },
              },
              _insurancePolicy: {
                _id: '',
                PolicyNumber: -1,
                Premium: -1,
                DiscountPercentage: -1,
                _policyHolder: {
                  _id: '',
                  IBAN: '',
                  FullName: '',
                  Initials: '',
                  Email: '',
                  Telephone: '',
                  Address: '',
                  Zipcode: '',
                  Lat: '',
                  Long: '',
                  _entityData: {
                    _changedById: -1,
                    LastChanged: null,
                  },
                },
                _entityData: {
                  _changedById: -1,
                  LastChanged: null,
                },
              },
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
          ]);
        })
      );
  }

  getInsurance(id: string) {
    return this.http
      .get<InsuranceResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurances/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            PaymentPeriodMonths: -1,
            PrevCollectionDate: null,
            NextCollectionDate: null,
            Description: '',
            _insurer: {
              _id: '',
              FullName: '',
              CompanyName: '',
              Address: '',
              Zipcode: '',
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _insuranceType: {
              _id: '',
              TypeName: '',
              PaymentPeriodMonths: -1,
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _insurancePolicy: {
              _id: '',
              PolicyNumber: -1,
              Premium: -1,
              DiscountPercentage: -1,
              _policyHolder: {
                _id: '',
                IBAN: '',
                FullName: '',
                Initials: '',
                Email: '',
                Telephone: '',
                Address: '',
                Zipcode: '',
                Lat: '',
                Long: '',
                _entityData: {
                  _changedById: -1,
                  LastChanged: null,
                },
              },
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  createInsurance(
    insurance: Insurance,
    defaultInsurerTypeID: string,
    defaultInsurancePolicyID: string
  ) {
    let data = {
      _insurer: insurance.Insurer.ID,
      _insuranceType: defaultInsurerTypeID,
      _insurancePolicy: defaultInsurancePolicyID,
      PaymentPeriodMonths: insurance.PaymentPeriodMonths,
      PrevCollectionDate: this.formatDateString(
        new Date(Date.parse(insurance.PrevCollectionDateEntry))
      ),
      NextCollectionDate: this.formatDateString(
        new Date(Date.parse(insurance.NextCollectionDateEntry))
      ),
      Description: insurance.Description,
    };

    console.log(data);
    return this.http
      .post<InsuranceResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurances/',
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            PaymentPeriodMonths: -1,
            PrevCollectionDate: null,
            NextCollectionDate: null,
            Description: '',
            _insurer: {
              _id: '',
              FullName: '',
              CompanyName: '',
              Address: '',
              Zipcode: '',
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _insuranceType: {
              _id: '',
              TypeName: '',
              PaymentPeriodMonths: -1,
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _insurancePolicy: {
              _id: '',
              PolicyNumber: -1,
              Premium: -1,
              DiscountPercentage: -1,
              _policyHolder: {
                _id: '',
                IBAN: '',
                FullName: '',
                Initials: '',
                Email: '',
                Telephone: '',
                Address: '',
                Zipcode: '',
                Lat: '',
                Long: '',
                _entityData: {
                  _changedById: -1,
                  LastChanged: null,
                },
              },
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  updateInsurance(
    id: string,
    insurance: Insurance,
    defaultInsurerTypeID: string,
    defaultInsurancePolicyID: string
  ) {
    let data = {
      _insurer: insurance.Insurer.ID,
      _insuranceType: defaultInsurerTypeID,
      _insurancePolicy: defaultInsurancePolicyID,
      PaymentPeriodMonths: insurance.PaymentPeriodMonths,
      PrevCollectionDate: this.formatDateString(
        new Date(Date.parse(insurance.PrevCollectionDateEntry))
      ),
      NextCollectionDate: this.formatDateString(
        new Date(Date.parse(insurance.NextCollectionDateEntry))
      ),
      Description: insurance.Description,
    };
    return this.http
      .put<InsuranceResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurances/' + id,
        data,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            PaymentPeriodMonths: -1,
            PrevCollectionDate: null,
            NextCollectionDate: null,
            Description: '',
            _insurer: {
              _id: '',
              FullName: '',
              CompanyName: '',
              Address: '',
              Zipcode: '',
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _insuranceType: {
              _id: '',
              TypeName: '',
              PaymentPeriodMonths: -1,
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _insurancePolicy: {
              _id: '',
              PolicyNumber: -1,
              Premium: -1,
              DiscountPercentage: -1,
              _policyHolder: {
                _id: '',
                IBAN: '',
                FullName: '',
                Initials: '',
                Email: '',
                Telephone: '',
                Address: '',
                Zipcode: '',
                Lat: '',
                Long: '',
                _entityData: {
                  _changedById: -1,
                  LastChanged: null,
                },
              },
              _entityData: {
                _changedById: -1,
                LastChanged: null,
              },
            },
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  deleteInsurance(id: string) {
    return this.http
      .delete<InsuranceResponse>(
        'https://cswp-individueel-api.herokuapp.com/api/v1/insurances/' + id,
        {
          headers: new HttpHeaders({
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Origin': '*',
            'X-JWT-Token': this.authService.getSession(),
          }),
        }
      )
      .pipe(
        catchError((e: HttpErrorResponse) => {
          this.handleError(e);
          return of({
            status: e.status,
            _id: '',
            PaymentPeriodMonths: -1,
            PrevCollectionDate: null,
            NextCollectionDate: null,
            Description: '',
            _insurer: null,
            _insuranceType: null,
            _insurancePolicy: null,
            _entityData: {
              _changedById: -1,
              LastChanged: null,
            },
          });
        })
      );
  }

  formatDateString(date: Date): string {
    return formatDate(date, 'yyy-MM-dd', 'en-US');
  }
}
