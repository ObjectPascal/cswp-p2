export interface InsuranceResponse {
  status: number;
  _id: string;
  PaymentPeriodMonths: number;
  PrevCollectionDate: Date;
  NextCollectionDate: Date;
  Description: string;
  _insurer: {
    _id: string;
    FullName: string;
    CompanyName: string;
    Address: string;
    Zipcode: string;
    _entityData: {
      _changedById: number;
      LastChanged: Date;
    };
  };
  _insuranceType: {
    _id: string;
    TypeName: string;
    PaymentPeriodMonths: number;
    _entityData: {
      _changedById: number;
      LastChanged: Date;
    };
  };
  _insurancePolicy: {
    _id: string;
    PolicyNumber: number;
    Premium: number;
    DiscountPercentage: number;
    _policyHolder: {
      _id: string;
      IBAN: string;
      FullName: string;
      Initials: string;
      Email: string;
      Telephone: string;
      Address: string;
      Zipcode: string;
      Lat: number;
      Long: number;
      _entityData: {
        _changedById: number;
        LastChanged: Date;
      };
    };
    _entityData: {
      _changedById: number;
      LastChanged: Date;
    };
  };
  _entityData: {
    _changedById: number;
    LastChanged: Date;
  };
}
